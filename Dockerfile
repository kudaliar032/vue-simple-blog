FROM node:12-alpine as builder
WORKDIR /app
COPY package.json package.json
COPY package-lock.json package-lock.json
RUN npm install
COPY . .
RUN npm run build

FROM nginx:alpine as hosting
COPY docker/nginx.conf /etc/nginx/nginx.conf
COPY --from=builder /app/dist /app
