FROM node:12-alpine as builder
WORKDIR /app
COPY . /app
RUN npm install
RUN npm run build

FROM nginx:alpine as hosting
COPY docker/nginx.conf /etc/nginx/nginx.conf
COPY --from=builder /app/dist /app
